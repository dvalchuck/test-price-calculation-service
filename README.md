# test-price-calculation-service

API service to calculate products price.

To start service `make run` in project folder.
Server start on 8080 port so application will be accessible thought `http://localhost:8080/`.

To get full price response it's necessary to add `product` GET parameter with HTML decoded JSON with products array.
Like this:
 `http://localhost:8080/?products=%5B%22ZA%22%2C%22YB%22%2C%22FC%22%2C%22GD%22%2C%22ZA%22%2C%22YB%22%2C%22ZA%22%2C%22ZA%22%5D`
Where `products` parameter is decoded ["ZA","YB","FC","GD","ZA","YB","ZA","ZA"] JSON.  

