service=test-price-calculation-service

install:
	@docker run --rm -v ${PWD}:/var/www/html $(service) composer update


docker-build:
	@docker build -t $(service) .

docker-build-dev:
	@docker build --build-arg XDEBUG_INSTALL=1 -t $(service) .

run: docker-build
	@docker run -p 8080:80 --name=$(service) --rm \
	    -e XDEBUG_CONFIG="remote_host=172.17.0.1" \
	    -v ${PWD}:/var/www/html \
	    -d \
	    $(service)

run-dev:
	@docker run -p 8080:80 --name=$(service) --rm \
		-e XDEBUG_CONFIG="remote_host=172.17.0.1" \
		-v ${PWD}:/var/www/html \
		-d \
		$(service)

stop:
	@docker stop $(service)
