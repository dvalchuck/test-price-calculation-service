<?php


namespace PriceCalculatorService\App;


use PriceCalculatorService\App\Exception\ApplicationException;
use PriceCalculatorService\App\Service\Request;
use PriceTerminal\Terminal;

/**
 * Request service.
 *
 * @property Request  request
 * @property Terminal priceTerminal
 */
class Application
{
	/**
	 * App configuration.
	 *
	 * @var array
	 */
	private $config;

	/**
	 * Services configuration.
	 *
	 * @var array
	 */
	private $servicesConfig;

	/**
	 * An array of registered Services.
	 *
	 * @var array
	 */
	private $services;

	/**
	 * Application constructor.
	 *
	 * @param array $config App configuration.
	 */
	public function __construct(array $config)
	{
		$this->config         = $config;
		$this->servicesConfig = $config['Services'];
	}

	/**
	 * Getter magic method.
	 * This method is overridden to support accessing application components.
	 *
	 * @param string $name Service or property name.
	 *
	 * @return mixed
	 * @throws ApplicationException
	 */
	public function __get(string $name)
	{
		if ($this->hasService($name)) {
			return $this->getService($name);
		}
		// Common getter.
		$getter = 'get' . $name;
		if (method_exists($this, $getter)) {
			return $this->$getter();
		}

		throw new ApplicationException('Property "' . get_class($this) . '.' . $name . '" is not defined.');
	}

	/**
	 * Checks whether the named service exists.
	 *
	 * @param string $id Service ID.
	 *
	 * @return boolean
	 */
	public function hasService(string $id): bool
	{
		return isset($this->services[$id]) || isset($this->servicesConfig[$id]);
	}

	/**
	 * Retrieves the named application service.
	 *
	 * @param string $id Service ID.
	 *
	 * @return mixed
	 * @throws ApplicationException
	 */
	public function getService($id)
	{
		if (isset($this->services[$id])) {
			return $this->services[$id];
		}
		elseif (isset($this->servicesConfig[$id])) {
			$config = $this->servicesConfig[$id];

			$component = $this->createService($config);

			$component->app = $this;

			return $this->services[$id] = $component;
		}

		throw new ApplicationException('Failed to load service with ID ' . $id . '.');
	}

	/**
	 * Creates service.
	 *
	 * @param array $config Service config.
	 *
	 * @return mixed
	 * @throws ApplicationException
	 */
	public static function createService($config)
	{
		if (isset($config['class'])) {
			$type = $config['class'];
			unset($config['class']);
		}
		else {
			throw new ApplicationException('Object configuration must be an array containing a "class" element.');
		}

		if (!class_exists($type, true)) {
			throw new ApplicationException('Failed to load ' . $type);
		}

		if (($n = count($config)) >= 1) {
			$args = array_values($config);
			if ($n === 1) {
				$object = new $type($args[0]);
			}
			elseif ($n === 2) {
				$object = new $type($args[0], $args[1]);
			}
			else {
				$object = new $type($args[0], $args[1], $args[2]);
			}
		}
		else {
			$object = new $type;
		}

		return $object;
	}
}