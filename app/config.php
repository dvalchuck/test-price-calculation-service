<?
// This is the main Web application configuration.
return [
	'Services' => [
		'request'       => [
			'class' => 'PriceCalculatorService\App\Service\Request',
			'rules' => [
				[
					'controller' => 'PriceCalculatorService\App\Controllers\IndexController',
					'action'     => 'indexAction',
					'pattern'    => '/\//',
					'verb'       => 'GET',
					'params'     => ['products']
				],
			]
		],
		'priceTerminal' => [
			'class'                 => 'PriceTerminal\Terminal',
			'productsConfiguration' => [
				'ZA' => ['price' => 2, 'volumePrices' => [4 => 7]],
				'YB' => ['price' => 12],
				'FC' => ['price' => 1.25, 'volumePrices' => [6 => 6]],
				'GD' => ['price' => 0.15]
			]
		]
	]
];
