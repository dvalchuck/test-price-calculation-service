<?php


namespace PriceCalculatorService\App\Exception;

/**
 * Class ApplicationException
 *
 * @package PriceCalculatorService\App\Exception
 */
class ApplicationException extends \Exception
{

}