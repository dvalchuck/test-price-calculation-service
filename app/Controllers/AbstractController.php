<?php


namespace PriceCalculatorService\App\Controllers;


use PriceCalculatorService\App\Application;

class AbstractController
{

	/**
	 * @var Application
	 */
	protected $app;

	public function __construct(Application $app)
	{
		$this->app = $app;
	}
}