<?php


namespace PriceCalculatorService\App\Controllers;


use PriceCalculatorService\App\Exception\ApplicationException;
use PriceTerminal\Exception\PriceTerminalException;

class IndexController extends AbstractController
{
	/**
	 * @param array $params
	 *
	 * @throws ApplicationException
	 */
	public function indexAction(array $params)
	{
		$products = @json_decode($params['products']);

		if (!$products) {
			throw new ApplicationException('Failed to parse product data.');
		}

		try {
			$this->app->priceTerminal->addProducts($products);
		} catch (PriceTerminalException $exception) {
			throw new ApplicationException($exception->getMessage());
		}

		$fullPrice = '£' . $this->app->priceTerminal->getTotal();
		$response  = ['fullPrice' => $fullPrice];

		header('HTTP/1.1 200');
		header('Content-type: application/json');

		echo json_encode($response, JSON_UNESCAPED_SLASHES);
	}

}