<?php


namespace PriceCalculatorService\App\Service;


use PriceCalculatorService\App\Application;
use PriceCalculatorService\App\Exception\ApplicationException;

/**
 * @property Application app
 */
class Request
{

	/**
	 * Url rules array.
	 *
	 * @var array
	 */
	private $rules;
	private $requestUri;

	public function __construct(array $rules)
	{
		$this->rules = $rules;
	}

	/**
	 * Processes the current request.
	 *
	 * @throws ApplicationException
	 */
	public function processRequest()
	{
		$requestUri = $this->getRequestUri();
		$processed  = false;
		foreach ($this->rules as $rule) {
			if ($processed = $this->parseUrl($requestUri, $rule)) {
				$this->runController($rule);
			}
		}

		if (!$processed) {
			throw new ApplicationException('Page not found.');
		}
	}

	/**
	 * Returns the request URI portion for the currently requested URL.
	 *
	 * @return string
	 * @throws ApplicationException
	 */
	protected function getRequestUri(): string
	{
		if ($this->requestUri === null) {
			if (isset($_SERVER['REQUEST_URI'])) {
				$this->requestUri = $_SERVER['REQUEST_URI'];
				if (!empty($_SERVER['HTTP_HOST'])) {
					if (strpos($this->requestUri, $_SERVER['HTTP_HOST']) !== false) {
						$this->requestUri = preg_replace('/^\w+:\/\/[^\/]+/', '', $this->requestUri);
					}
				}
				else {
					$this->requestUri = preg_replace('/^(http|https):\/\/[^\/]+/i', '', $this->requestUri);
				}
			}
			else {
				throw new ApplicationException('Application is unable to determine the request URI.');
			}
		}

		if (($pos = strpos($this->requestUri, '?')) !== false) {
			$this->requestUri = substr($this->requestUri, 0, $pos);
		}

		return $this->requestUri;
	}

	/**
	 * Parses a URL based on this rule.
	 *
	 * @param string $requestUri Requested url,
	 * @param array  $rule       Url rule.
	 *
	 * @return boolean
	 */
	protected function parseUrl($requestUri, $rule): bool
	{
		if (isset($rule['verb']) && $this->getRequestType() !== $rule['verb']) {
			return false;
		}

		if (preg_match($rule['pattern'], $requestUri, $matches)) {
			foreach ($rule['params'] as $key => $name) {
				if (!isset($_GET[$name])) {
					return false;
				}
			}
			if ($requestUri !== $matches[0]) // there're additional params
			{
				return false;
			}
		}
		else {
			return false;
		}

		return true;
	}

	/**
	 * Returns the request type, such as GET, POST, HEAD, PUT, PATCH, DELETE.
	 *
	 * @return string.
	 */
	public function getRequestType(): string
	{
		if (isset($_POST['_method'])) {
			return strtoupper($_POST['_method']);
		}
		elseif (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])) {
			return strtoupper($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']);
		}

		return strtoupper(isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET');
	}

	private function runController($rule)
	{
		$controller = new $rule['controller']($this->app);
		$params     = [];
		foreach ($rule['params'] as $key => $name) {
			if (isset($_GET[$name])) {
				$params[$name] = $_GET[$name];
			}
		}
		$controller->{$rule['action']}($params);
	}

}