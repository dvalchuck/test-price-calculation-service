<?php
$file     = __DIR__ . '/../vendor/autoload.php';
$autoload = require_once $file;

use PriceCalculatorService\App\Application;
use PriceCalculatorService\App\Exception\ApplicationException;

$config = require(dirname(__FILE__) . '/../app/config.php');
$app    = new Application($config);
try {
	$app->request->processRequest();
} catch (ApplicationException $exception) {
	header('HTTP/1.1 200');
	header('Content-type: application/json');

	$response = ['error' => $exception->getMessage()];
	echo json_encode($response, JSON_UNESCAPED_SLASHES);
}

die();